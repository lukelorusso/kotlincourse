fun main(args: Array<String>) {
    typesAndOperations()
    basicFlowControl()
}

fun typesAndOperations() {
    val pi = 3.142 // constant: cannot be changed
    println(pi)

    var age = 30 // standard variable
    age += 1
    println(age)

    val money = 34.56
    val no: Int = money.toInt() // some conversions
    println(no)

    val no1 = 10
    val no2 = 34.56

    println(no1 * no2)
    println(no1 + no2)

    // Dollar + Curly
    val n1 = 10
    val n2 = 23
    val result = "n1 * n2 = ${n1 * n2}"
    println(result)

    // Strings
    val name = "Luke"
    val greeting = "Hello $name"
    println(greeting)

    // Multiline String
    val bigString = """This is line 1
...than the second...
...e infine la terza!"""
    println(bigString)

    // a Pair
    val coordinates = Pair(3, 5)
    println(coordinates.first)
    println(coordinates.second)

    // it's STILL a Pair!
    val notARange = 3 to 5
    println(notARange.first)
    println(notARange.second)

    // assigning the pair to variables
    val (x, y) = coordinates
    println(x)
    println(y)

    // a Triple
    val coordinates3D = Triple(2, 3, 4)

    val (a, _, b) = coordinates3D // we are IGNORING the second value!
    println(a)
    println(b)
}

fun basicFlowControl() {
    val age = 14
    val major = 18
    val old = 85

    if (age < major) {
        println("less than $major")
    } else if (age > old) {
        println("more than $old years old")
    } else {
        println("greater or equal to $major")
    }

    var choice = " "
    while(choice != "q") {
        print("Enter your choice or press q to exit: ")
        choice = readLine().orEmpty()
    }
    println("You choose q!")

    for (index in 0..10 step 2) { // pay attention to the "step"
        println(index)
    }
}
