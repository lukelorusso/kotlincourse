fun main(args: Array<String>) {
    arrays()
    println("---")
    lists()
    println("---")
    sorting()
}

fun arrays() {
    val arr = arrayOf(3, 4, 5, 6, 7, 8, "Alex", "Mary")
    //val arr = intArrayOf(2, 4, 6, 8, 10) // int only
    //val arr = Array(5, {"Luke"}) // to init an array with the same value n times
    for (index in 0 until arr.size) { // "until anything.size" is the replacement for "0..anything.size - 1"
        println(arr[index])
    }
}

fun lists() {
    // Those lists are IMMUTABLE
    val names = listOf("Luke", "Alex", "Mary", "John")
    //val names: List<String> = listOf() // an empty string list
    //val names = listOf<String>() // an empty string with type specification (limitation)

    // Those lists are MUTABLE
    val members = mutableListOf<String>() // an empty typed mutable list
    members.add("John")
    members.add("Luke")
    members.add("Mary")
    members.add("Steve")
    val slicedMembers = members.slice(0..2) // returns a list containing elements at indices in the specified range

    if (slicedMembers.isEmpty()) {
        println("member list is empty!")
    } else {
        println(slicedMembers.first())
        println(slicedMembers[1])
        println(slicedMembers.last())
        println("Steve" in slicedMembers) //println(slicedMembers.contains("Steve"))
    }
}

fun sorting() {
    val names = listOf("Luke", "Alex", "Mary", "John") //arrayOf("Luke", "Alex", "Mary", "John")
    println(names.sorted())
    println(names.sortedDescending())
}
