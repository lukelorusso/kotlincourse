fun main(args: Array<String>) {
    val string = "Dog"

    when(string) { // it's like a switch-case...
        "Dog" -> println("It's a dog!")
        "Cat" -> println("It's a cat!")
        else -> println("It's something...")
    }

    val age = 15

    println(when(age) { // ...but more powerful!
        in 0..2 -> "Infant"
        in 3..12 -> "Child"
        in 13..19 -> "Teen"
        else -> "Adult"
    })
}