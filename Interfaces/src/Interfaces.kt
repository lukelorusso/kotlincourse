fun main(arga: Array<String>) {
    val car = Car()
    car.start()
    car.changeGear(3) // this will override the default value of gear
    car.move()
}

interface Movable {
    fun move()
}

interface Vehicle {
    fun start() { // default implementation
        println("started inside the interface!")
    }

    fun changeGear(gear: Int = 1) // default value of gear = 1
}

class Car: Vehicle, Movable {
    override fun move() {
        println("accelerating...")
    }

    override fun start() { // if not implemented, the default implementation will be called
        super.start()
        println("started inside the class!")
    }

    override fun changeGear(gear: Int) {
        println("changed to: $gear")
    }

}
