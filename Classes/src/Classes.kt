
fun main(args: Array<String>) {
    basics()
    println("---")
    comparingInstances()
}

fun basics() {
    val customer1 = Customer("John", "Doe") // an instance of the class
    println(customer1.fullName)

    var customer2 = customer1
    println(customer1 === customer2) // those are pointers to the same object

    val customer3 = Customer("Luke", "FileWalker")
    customer2 = customer3
    println(customer1 === customer2) // not the same object anymore
}

fun comparingInstances() {
    val coordinate1 = Coordinate(95.5, 35.5)
    val coordinate2 = Coordinate(95.5, 35.5)
    println(coordinate1 === coordinate2) // not the same objects, even with same values
    println(coordinate1 == coordinate2) // because of the "data class" definition, they ARE equals!
}
