class Customer(
    var firstName: String,
    var lastName: String
) {
    val fullName get() = "$firstName $lastName" // a "computed" property
}
