import kotlin.math.sqrt

fun main(args: Array<String>) {
    val square = Square(25.0)
    square.area = 100.0 // you can't change it, unless you create a SETTER too
    println(square.width)
}

class Square(var width: Double) {
    var area: Double
        get() {
            return width * width
        }
        set(value) {
            width = sqrt(value)//value / 2
        }
}
