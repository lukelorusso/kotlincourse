fun main(args: Array<String>) {
    /* palindrome: a word or sequence that reads the same backwards as forwards:
    mom, madam ... */

    val someString1 = "Abacaba"
    println(someString1.isPalindrome)

    val someString2 = "Cat"
    println(someString2.isPalindrome)
}

val String.isPalindrome: Boolean
    get() {
        return this.toLowerCase().reversed() == this.toLowerCase()
    }
