import kotlin.properties.Delegates

fun main(args: Array<String>) {
    val shirt = Shirt()
    shirt.sold = true
    println(Shirt.totalSold)

    val shirt2 = Shirt()
    shirt2.sold = true
    println(Shirt.totalSold)
}

class Shirt {
    companion object { // our static object
        var totalSold = 0
    }

    var sold: Boolean by Delegates.observable(false) {
        _, old, new -> // _ is the object itself, which we don't want
        if (new) {
            totalSold += 1 // will be triggered when a new value will be assigned to "sold" object!
        }
    }
}
