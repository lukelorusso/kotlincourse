import kotlin.math.sqrt

fun main(args: Array<String>) {
    val t = Triangle(10.0, 25.0)
    println(t.hypotenuse) // Initialization would be run just NOW!!

}

/**
 * by lazy { ... } performs its initializer when the defined property is first used,
 * not its declaration.
 */
class Triangle(var sideA: Double, var sideB: Double) {
    val hypotenuse: Double by lazy { // needs to be val!!!
        sqrt((sideA * sideB) + (sideB * sideB))
    }
}
