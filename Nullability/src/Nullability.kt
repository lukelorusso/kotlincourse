fun main(args: Array<String>) {
    intro()
    checkingForNull()
    safeCalls()
    elvisOperator()
    println(divideIfWhole(10, 5))
}

fun intro() {
    val age: Int? = 10 // ? means it can have null as value
    println(age)

    var parseInt = "30".toIntOrNull()
    println(parseInt)

    parseInt = "abc".toIntOrNull() // what will it be?
    println(parseInt)
}

fun checkingForNull() {
    var age: Int? = 32

    //age += 1 // you're NOT allowed to increment a nullable!
    //age = age + 1 // same thing here
    //age = age!! + 1 // this is acceptable: putting age's value "out of the protection box" (force unwrapping)
    if (age != null) age += 1 // this is ALSO acceptable: we are checking the nullability
    println(age)
}

fun safeCalls() {
    val middleName: String? = "John"

    println(middleName?.length) // access the "length" property ONLY if the val is NOT null

    middleName?.let {
        val len = it.length // "it" refers to middleName!
        println(len)
    }
}

fun elvisOperator() { // Exercise: try to answer why it's called "Elvis" xD
    val age: Int? = 10

    println(age ?: 0) // if NOT null print age, otherwise print 0
}

fun divideIfWhole(value: Int, divisor: Int): Int? { // this way you authorize null as a possible return value
    val quotient = value / divisor
    val remainder = value % divisor
    return if (remainder == 0) quotient else null
}