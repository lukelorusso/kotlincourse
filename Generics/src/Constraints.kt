fun main(args: Array<String>) {
    println(swap(10, "forty-five"))
}

fun<A, B> swap(a: A, b: B): Pair<B, A> {
    return Pair(b, a)
}
