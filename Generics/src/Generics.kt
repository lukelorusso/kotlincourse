fun main(args: Array<String>) {
    val list = mutableListOf<Any>() // I can say it's the counterpart of Object in Java...
    list.add("aaa")
    list.add(3)

    val map = mutableMapOf<String, Any>() // can also be <Any, Any>
    map["a"] = 1
    map["b"] = "due"

    val names = listOf("Alex", "John", "Luke")
    val numbers = listOf(2, 3, 5, 7, 11, 13, 17, 19, 23, 29)
    println(names.toBulletList())
    println(numbers.toBulletList())
}

fun List<Any>.toBulletList(): Any {
    val prefix = "\n * "
    val postfix = "\n"
    return this.joinToString(prefix, prefix = prefix, postfix = postfix)
}
