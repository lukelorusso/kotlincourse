fun main(args: Array<String>) {
    //val user = User("John", "Doe") // can't do this: the constructor is PRIVATE
    val user1 = User.newUser("John", "Doe")
    val user2 = User.newUser("Mary", "Doe")
    println(User.numberOfUsers)
}

class User private constructor(
    var firstName: String,
    var lastname: String
) {
    companion object { // it's like a static object
        var numberOfUsers: Int = 0

        fun newUser(firstName: String, lastname: String): User {
            numberOfUsers += 1
            return User(firstName, lastname)
        }
    }
}
