fun main(args: Array<String>) {
    val customer = Customer("John", "Doe")
    CustomRepository.addCustomer(customer)
}

data class Customer(var firstName: String, var lastName: String) {
    // nothing more needed  
}

object CustomRepository { // it's a singleton!
    val allCustomers = mutableListOf<Customer>()

    fun addCustomer(customer: Customer) {
        allCustomers.add(customer)
    }
}
