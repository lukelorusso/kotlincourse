fun main(args: Array<String>) {
    examples()
    println("---")
    scope()

}

fun examples() {
    val addLambda: (Int, Int) -> Int // this is just a declaration of a lambda
    addLambda = { a: Int, b: Int -> Int
        a + b
    } // here we are specifying the lambda body
    println(addLambda(2, 5))

    val squareLambda = { a: Int ->
        a * a
    } // a shorter way
    println(squareLambda(5))

    val anotherSquareLambda: (Int) -> Int = {
        it * it
    } // same meaning as before, maybe more common in other languages
    println(anotherSquareLambda(5))

    printResult(3, 5, addLambda) // lambdas can be arguments!!!

    val numbers = arrayOf(4, 56, 34, 12, 1)
    numbers.forEach {
        print(it)
        print(" ")
    } // a forEach function
    print("\n")

    val mappedNumbers = numbers.map { it * 2 } // a map function
    println(mappedNumbers)

    val largerThan5 = numbers.filter { it > 5 } // a filter function
    println(largerThan5)
}

fun printResult(a: Int, b: Int, operation: (Int, Int) -> Int) {
    println(operation(a, b))
}

fun scope() {
    var counter = 0
    val incrementCounter = {
        counter++
    }

    incrementCounter()
    incrementCounter()
    println(counter) // we can access the value of counter

    val counter1 = countingLambda() // this is referred to another scope
    println(counter1())
    println(counter1())
}

fun countingLambda(): () -> Int {
    var counter = 0
    val incrementCounter: () -> Int = {
        counter++
        counter
    }
    return incrementCounter
}
