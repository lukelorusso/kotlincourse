fun main(args: Array<String>) {
    maps()
    println("---")
    sets()
}

fun maps() {
    val languages = mapOf(
        "it" to "Italiano",
        "fr" to "Français",
        "en" to "English"
    )
    println(languages["IT".toLowerCase()])

    val mutableLanguages = mutableMapOf(
        "it" to "Italiano",
        "fr" to "Français",
        "en" to "English"
    )
    mutableLanguages["es"] = "Español" // add
    mutableLanguages.remove("fr") // remove
    println(mutableLanguages["es"])

    for (key in mutableLanguages.keys) {
        println(key)
    }

    for (value in mutableLanguages.values) {
        println(value)
    }

    for ((code, lang) in mutableLanguages) {
        println("$code: $lang")
    }
}

fun sets() {
    val names = setOf(
        "a", "b", "c", "d"
    )
    println(names)

    val numbers = arrayOf(1, 2, 3, 1, 5, 6) // pay attention to the repeated "1"
    val numberSet = mutableSetOf(*numbers) // the spread operator
    numberSet.add(12)
    numberSet.remove(5)

    println(numberSet)
}
