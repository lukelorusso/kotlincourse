fun main(args: Array<String>) {
    basics()
    println("---")
    propertiesAndFunctions()
}

fun basics() {
    for (direction in Compass.values()) {
        println("Index is ${direction.ordinal} and name is ${direction.name}")
    }

    val compass = Compass.North
    print(compass)
    println(when(compass) {
        Compass.North -> " is the north"
        Compass.East -> " is the east"
        Compass.South -> " is the south"
        Compass.West -> " is the west"
    })
}

fun propertiesAndFunctions() {
    for (transport in Transport.values()) {
        val text: String = if (transport.hasWheels)
            "has wheels"
        else
            "has NO wheels"
        println("${transport.name} $text")
    }

    println("\nCurrent transport selected: ${Transport.current()}")
}

enum class Compass {
    North,
    East,
    South,
    West
}

enum class Transport(var hasWheels: Boolean = true) {
    TucTuc,
    JetPack(false),
    Motorbike,
    Ship(false); // yes... ; is back!

    companion object {
        fun current(): Transport {
            val index = 1
            return Transport.values()[index]
        }
    }
}
