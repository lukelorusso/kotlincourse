fun main(args: Array<String>) {
    val closedRange = 0..5 // 0 1 2 3 4 5
	for (index in closedRange) {
	    println(index)
	}
	println("-")
	
	val decreasingRange = 5 downTo 0 // 5 4 3 2 1
	for (index in decreasingRange) {
	    println(index)
	}
	println("-")
	
	val halfOpenRange = 0 until 5 // 0 1 2 3 4
	for (index in halfOpenRange) {
	    println(index)
	}
	println("-")
}