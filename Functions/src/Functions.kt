fun main(args: Array<String>) {
    val myF = ::add // this is a pointer to a function
    val myOtherF = ::square

    displayResult(4, 5, myF) // here we are also passing the pointer as an argument!
    //displayResult(4, 5, myOtherF) // this fun cannot be passed to displayResult because it takes only 1 arg, instead of 2 as defined in the displayResult sign
}

fun displayResult(a: Int, b: Int, function: (Int, Int) -> Int) {
    val result = function(a, b) // here we are invoking the function taken from the argument
    println(result)
}

fun add(a: Int, b: Int): Int {
    return a + b
}

fun square(a: Int): Int {
    return a * a
}
