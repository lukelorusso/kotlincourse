fun main(args: Array<String>) {
    val citroen = CityCar("Citroen", "C1")
    startUpCar(citroen)
    citroen.performService()

    println("---")

    val ferrari = SportCar("Ferrari", "GTC4")
    startUpCar(ferrari)
    ferrari.performService()
}

fun startUpCar(car: Car) {
    println(car.start())
    if (car is SportCar) {
        println("it's a Sportcar!")
    }
    (car as? SportCar)?.isTurboOn = true
}

abstract class Car(var maker: String, var model: String) {
    abstract fun start(): String

    open fun performService() {
        println("Tire check")
        println("Oil check")
        println("Breaks check")
    }
}

open class CityCar(maker: String, model: String): Car(maker, model) {
    override fun start(): String {
        return "brum"
    }
}

class SportCar(maker: String, model: String): Car(maker, model) {
    override fun start(): String {
        return "wroooom"
    }

    override fun performService() {
        super.performService() // you have to specify if calling super or not!
        println("Micro-filter check")
    }

    var isTurboOn = false
}
