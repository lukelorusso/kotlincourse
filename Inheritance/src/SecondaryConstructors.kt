fun main(args: Array<String>) {
    val person = Person("John", "Foo", "Doe")
    println(person.middleName)
}

class Person(var firstName: String, var lastName: String) { // default constructor
    var middleName: String = ""

    constructor(firstName: String, middleName: String, lastName: String): this(firstName, lastName) { // second constructor
        this.middleName = middleName
    }
}
