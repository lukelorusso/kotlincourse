fun main(args: Array<String>) {
    // multiple of 3 then write Fizz
    // multiple of 5 then write Buzz
    // multiple of 3 and 5 then write FizzBuzz
    val number = if (args.isNotEmpty()) args[0].toInt() else 15

    /*if (number % 3 == 0) {
        print("Fizz")
    }
    if (number % 5 == 0) {
        print("Buzz")
    }*/

    println(when {
        number % 3 == 0 && number % 5 == 0 -> "FizzBuzz"
        number % 3 == 0 -> "Fizz"
        number % 5 == 0 -> "Buzz"
        else -> "something else"
    })
}